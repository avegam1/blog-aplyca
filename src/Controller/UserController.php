<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Form\RegisterType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    
    public function register(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
       //crear el formulario
    	$user=new User();
    	$form=$this->createForm(RegisterType::class,$user);
        //rellenar el objeto con los datos del formulario
    	$form->handleRequest($request);
    	//comprobar si se envio el formulario
    	if($form->isSubmitted() && $form->isValid()){
    		//modificando el objeto para guardarlo
            //$user->setRole('role_user');
    		$user->setCreateAt(new \Datetime('now'));
    		//cifrando la contraseña
    		$encod=$encoder->encodePassword($user, $user->getPassword());
    		$user->setPassword($encod);

    		//guardar el usuario
    		 $em= $this->getDoctrine()->getManager();
    		 $em->persist($user);
    		 $em->flush();
    		return $this->redirectToRoute('post');

    	}

        return $this->render('user/register.html.twig', [
            'form'=> $form-> createView()
        ]);

        
    }
	public function login(AuthenticationUtils $authenticationUtils){
    	$error = $authenticationUtils->getLastAuthenticationError();
    	 $lastUsername = $authenticationUtils->getLastUsername();
		 //return $this->redirectToRoute('post');
    	return $this->render('user/login.html.twig', array(
    		'error'=>$error,
    		'last_username'=>$lastUsername

    	));
	}
}
