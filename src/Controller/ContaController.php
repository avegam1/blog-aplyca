<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Contact;
use App\Form\ContactoType;

class ContaController extends AbstractController
{
    
    public function contacto(Request $request){
    	$contacto = new Contact();
    	$form=$this->createForm(ContactoType::class,$contacto);
    	$form->handleRequest($request);
    	

    	if($form->isSubmitted() && $form->isValid()){
    		
    		//guardar el usuario
    		 $em1= $this->getDoctrine()->getManager();
    		 $em1->persist($contacto);
    		 $em1->flush();
    		return $this->redirectToRoute('post');

    	}
    	return $this->render('conta/contacto.html.twig', [
        	'form'=> $form-> createView()
            
        ]);

    }
}
