<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Post;
use App\Entity\User;
use App\Form\PostType;
use App\Form\UpdateType;
//use App\Entity\UserInterface;


class PostController extends AbstractController
{
    
    public function index(): Response
    {
       
        //probando las entidades y relaciones(sacar todos los post de la bd)
        $entitymanager=$this->getDoctrine()->getManager();

        #obteniendo los repositorios

        $post_repositorio=$this->getDoctrine()->getRepository(Post::class);

        #obteniendo todas las entradas del blog

        $posts=$post_repositorio->findBy([],['id'=>'DESC']);
        
        //prueba de extrar todo los usuarios y las entradas adjuntas a ellos

        /*$user_repos=$this->getDoctrine()->getRepository(User::class);
    	$users=$user_repos->findALL();
    	foreach ($users as $user) {
    		
    		echo "<h4>{$user->getnombre()} {$user->getapellidos()}</4>";
    		foreach ($user->getposts() as $post) {
    		
    		echo $post->getTitulo()."</br>";
    	}
    }*/


        return $this->render('post/index.html.twig', [
            'post' =>$posts,
        ]);
    }
    public function home (Request $request){
        
        return $this->render('post/home.html.twig', [
           
        ]);
    }
    public function detail(Post $post){
        if(!$post){
            return $this->redirectToRoute('post');
        }
        return $this->render('post/detail.html.twig', [
            'post' =>$post,
        ]);
    }
    public function creation(Request $request, SluggerInterface $slugger, UserInterface $user){
        $post=new Post();
    	$form=$this->createForm(PostType::class,$post);
    	$form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //$user=getUser();
            $post->setUser($user);
            $user->setCreateAt(new \Datetime('now')); 
            $brochureFile = $form->get('foto')->getData();
            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $brochureFile->move(
                        $this->getParameter('fotos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('message:lo siento archivo fallido');
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $post->setFoto($newFilename);
            }
             $em= $this->getDoctrine()->getManager();
    		 $em->persist($post);
    		 $em->flush();

        }
        //return $this->redirectToRoute('post_creation');
        //return $this->redirect($this->generateUrl('post_detail', ['id'=> $post->getId()]));
       
        return $this->render('post/creacion.html.twig', [
            'form'=> $form-> createView()
        ]);
    }
    public function mypost (UserInterface $user){
        $post=$user->getPosts();
        return $this->render('post/mypost.html.twig', [
            'mypost' =>$posts,
        ]);
        
    }
    public function allpost (){
        $entitymanager=$this->getDoctrine()->getManager();
        $post_repositorio=$this->getDoctrine()->getRepository(Post::class);
        $posts=$post_repositorio->findBy([],['id'=>'DESC']);
        /*$user_repos=$this->getDoctrine()->getRepository(User::class);
    	$users=$user_repos->findALL();
    	foreach ($users as $user) {
    		
    		echo "<h4>{$user->getnombre()} {$user->getapellidos()}</4>";
    		foreach ($user->getposts() as $post) {
    		
    		echo $post->getTitulo()."</br>";
    	}
    }*/
        return $this->render('post/allpost.html.twig',[
            'mypost' =>$posts,
        ]);
        
    }
    public function edit (Request $request, Post $post, UserInterface $user ){
        if(!$user || $user->getId() !=$post->getUser()->getId()){
            return $this->redirectToRoute('post');
        }
        $post=new Post();
    	$form=$this->createForm(UpdateType::class,$post);
    	$form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //$post->setUser($user);
            //$user->setCreateAt(new \Datetime('now')); 
            $brochureFile = $form->get('foto')->getData();
            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $brochureFile->move(
                        $this->getParameter('fotos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('message:lo siento archivo fallido');
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $post->setFoto($newFilename);
            }
             $em= $this->getDoctrine()->getManager();
    		 $em->persist($post);
    		 $em->flush();

        }
        
        return $this->render('post/edit.html.twig',[
            'edit' =>$posts,
            'form'=> $form-> createView()
        ]);
        
    }
    

        

    
}