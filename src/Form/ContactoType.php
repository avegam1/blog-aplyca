<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use  Symfony\Component\Form\Extension\Core\Type\TextType;
use  Symfony\Component\Form\Extension\Core\Type\EmailType;
use  Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ContactoType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		# code...
		$builder->add('email',EmailType::class,array(
			'label'=>'Email'
			))
			->add('nombres',TextType::class,array(
			'label'=>'nombres'
			))
			->add('mensaje',TextType::class,array(
			'label'=>'Mensaje'
			))
			->add('submit',SubmitType::class,array(
			'label'=>'Enviar Contacto'
			));
	}
}