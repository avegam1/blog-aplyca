<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use  Symfony\Component\Form\Extension\Core\Type\TextType;
use  Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use  Symfony\Component\Form\Extension\Core\Type\SubmitType;


class UpdateType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		
		$builder->add('titulo',TextType::class,array(
			'label'=>'Nombre'
			))
			->add('foto',FileType::class,array(
			'label'=>'Selecciona una Imagen'
			))
			->add('contenido',TextareaType::class,array(
			'label'=>'Contenido'
            ))
            ->add('submit',SubmitType::class,array(
                'label'=>'Actulizar'
            ));
			
	}
}